# Assign the URL to the CSV file
data_url <- 'http://assets.datacamp.com/course/compfin/sbuxPrices.csv'

# Load the data frame using read.csv
sbux_df <- read.csv(data_url, header = TRUE, stringsAsFactors= FALSE)
  
  #https://www.rdocumentation.org/packages/utils/versions/3.3.3/topics/read.table