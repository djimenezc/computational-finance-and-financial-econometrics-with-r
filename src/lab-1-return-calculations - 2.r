# The sbux_df data frame is already loaded in your work space
#http://stackoverflow.com/questions/1295955/what-is-the-most-useful-r-trick

# Check the structure of sbux_df
str(sbux_df)

# Check the first and last part of sbux_df
head(sbux_df)
tail(sbux_df)


# Get the class of the Date column of sbux_df
class(sbux_df['Date'])
class(sbux_df$Date)
